FROM node:10.21.0-alpine

COPY . /src

VOLUME [".", "/src"]

WORKDIR src

RUN npm config set registry http://registry.npm.taobao.org/ && npm i -g eslint

CMD rm -rf node_modules package-loack.json && npm i -d && npm run build && npm run start

EXPOSE 3000