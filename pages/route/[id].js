import React from 'react'
import { useRouter } from 'next/router'

function RouterParam() {
  const router = useRouter()
  const { id } = router.query
  return <div>this is - {id}</div>
}

export default RouterParam