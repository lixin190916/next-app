import fs from 'fs'
import path from 'path'
import React from 'react'
import process from 'process'
import PropTypes from 'prop-types'

function ReadingFile({ posts }) {
  return (
    <ul>
      {posts.map((post) => (
        <li key={post.filename}>
          <h3>{post.filename}</h3>
          <p>{post.content}</p>
        </li>
      ))}
    </ul>
  )
}

export async function getStaticProps() {
  const postsDirectory = path.join(process.cwd(), 'pages/route')
  const filenames = await fs.readdirSync(postsDirectory)

  const posts = filenames.map((filename) => {
    const filePath = path.join(postsDirectory, filename)
    const fileContents = fs.readFileSync(filePath, 'utf8')

    return {
      filename,
      content: fileContents
    }
  })

  return {
    props: {
      posts
    }
  }
}

ReadingFile.propTypes = {
  posts: PropTypes.array
}

export default ReadingFile