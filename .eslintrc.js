module.exports = {
    'env': {
        'browser': true,
        'es2021': true
    },
    "extends": [
        "eslint:recommended",
        "plugin:react/recommended"
    ],
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 12,
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    'rules': {
        'no-console': "error",
        "no-irregular-whitespace": "error",
        "no-unreachable": "error",
        "default-case": "error",
        "require-await": "error",
        "no-use-before-define": "error",
        "array-bracket-spacing": ["error", "never"],
        "comma-dangle": ["error", "never"],
        "eol-last": ["error", "never"],
        "no-trailing-spaces": "error",
        "quotes": ["error", "single"],
        "semi": ["error", "never"],
        "no-var": "error",
    },
    "settings": {
        "react": {
          "version": "detect"
        }
      }
};
